use rmp::decode::{MarkerReadError, ValueReadError, RmpReadErr};

pub mod value;
pub mod value_ref;

pub use self::value::{read_value, read_value_with_max_depth};
pub use self::value_ref::{read_value_ref, read_value_ref_with_max_depth};
use alloc::string::String;
use alloc::format;

/// The maximum recursion depth before [`Error::DepthLimitExceeded`] is returned.
pub const MAX_DEPTH: usize = 1024;

/// This type represents all possible errors that can occur when deserializing a value.
#[derive(Debug)]
pub
enum Error
{
    /// Error while reading marker byte.
    InvalidMarkerRead(String),
    /// Error while reading data.
    InvalidDataRead(String),
    /// The depth limit [`MAX_DEPTH`] was exceeded.
    DepthLimitExceeded,
}

fn decrement_depth(depth: usize) -> Result<usize, Error>{
    if depth == 0 {
        Err(Error::DepthLimitExceeded)
    } else {
        Ok(depth - 1)
    }
}

impl<T: RmpReadErr> From<ValueReadError<T>> for Error {
    fn from(e: ValueReadError<T>) -> Self {
        Error::InvalidDataRead(format!("{:?}", e))
    }
}

impl<T: RmpReadErr> From<MarkerReadError<T>> for Error {
    fn from(e: MarkerReadError<T>) -> Self {
        Error::InvalidMarkerRead(format!("{:?}", e))
    }
}
