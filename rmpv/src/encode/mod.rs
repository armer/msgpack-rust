mod value;
mod value_ref;

pub use self::value::write_value;
pub use self::value_ref::write_value_ref;
use alloc::string::String;
use rmp::encode::{RmpWriteErr, ValueWriteError, MarkerWriteError};
use alloc::format;

#[derive(Debug)]
pub enum Error
{
    /// Error while reading marker byte.
    InvalidMarkerWrite(String),
    /// Error while reading data.
    InvalidDataWrite(String),
}

impl<T: RmpWriteErr> From<ValueWriteError<T>> for Error {
    fn from(e: ValueWriteError<T>) -> Self {
        match e {
            ValueWriteError::InvalidDataWrite(e) => Error::InvalidDataWrite(format!("{:?}", e)),
            ValueWriteError::InvalidMarkerWrite(e) => Error::InvalidMarkerWrite(format!("{:?}",e)),
        }
    }
}

impl<T: RmpWriteErr> From<MarkerWriteError<T>> for Error {
    fn from(e: MarkerWriteError<T>) -> Self {
        Error::InvalidMarkerWrite(format!("{:?}", e))
    }
}
